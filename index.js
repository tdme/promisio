/**
 * TDMe Coommand Line Interface
 *
 * @type {exports}
 */

var fs = require("fs"),
    path = require("path"),
    mkdirp = require('mkdirp'),
    easyProcess = require('easy-process'),
    Mustache = require('mustache'),
    ncp = require('ncp').ncp
    ;

function templateToFile(dir, filename, template, values) {
    return new Promise(function (onSucced, onRejected) {
        mkdirp(dir, function(err, data){
            if(err) onRejected(err);
            else {
                fs.readFile(template, function (err, data) {
                    if(err) onRejected(err);
                    else {
                        var modelFile = Mustache.render(data.toString(), values);
                        fs.writeFile(path.join(dir, filename), modelFile, function(err){
                            if(err) onRejected(err);
                            else {
                                onSucced(err);
                            }
                        })
                    }
                });
            }
        })
    });
}

function readFile(fileName, options) {
  return new Promise(function (onSucced, onRejected) {
    fs.readFile(fileName, options, function (err, data) {
      err ? onRejected(err) : onSucced(data);
    });
  })
};

function writeFile(fileName, content) {
  return new Promise(function (onSucced, onRejected) {
    fs.writeFile(fileName, content, function (err) {
      err ? onRejected(err) : onSucced(err);
    });
  })
};

function makeDirp(dirName) {
  return new Promise(function (onSucced, onRejected) {
    mkdirp(dirName, function (err) {
      err ? onRejected(err) : onSucced(err);
    });
  })
};

function copyRecursive (source, destination) {
    return new Promise(function(resolve, reject) {
        ncp(source, destination, function (err) {
            if(err) reject(err);
            resolve();
        });

    });
}

function runCmd(command, message, verbose) {
    return new Promise(function (onSucced, onRejected) {
        // Execute command..
        easyProcess.run(command
            , verbose
            , function(err, stdout, stderr) {
                err ? onRejected(err, stdout, stderr) : onSucced(err, stdout, stderr);
            }
        );
    })
}

module.exports = {
    'fs': {
        'templateToFile': templateToFile,
        'readFile': readFile,
        'writeFile': writeFile,
        'makeDirp': makeDirp,
        'copyRecursive': copyRecursive
    },
    'os': {
        'runCmd': runCmd
    }
}
