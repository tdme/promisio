[![Npm version](https://badge.fury.io/js/promisio.svg)](https://badge.fury.io/js/promisio)

# [Promisio](https://www.npmjs.com/package/promisio)

> A **Promises** implementation of **routinary tasks**,
 see more at [Promisio Npm](https://www.npmjs.com/package/promisio).


## Comming soon more..
We are working hard  to release the first version.
